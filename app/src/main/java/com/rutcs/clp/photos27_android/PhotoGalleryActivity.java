package com.rutcs.clp.photos27_android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.List;

public class PhotoGalleryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_gallery);
        AlbumManager.readAlbums("albums.txt",this);
        if(AlbumManager.albumsExist()){
            ListView albumList = findViewById(R.id.albumList);
            albumList.setVisibility(View.VISIBLE);
            FrameLayout emptyAlbums = findViewById(R.id.noAlbum_Frame);
            emptyAlbums.setVisibility(View.GONE);
        } else {
            ListView albumList = findViewById(R.id.albumList);
            albumList.setVisibility(View.GONE);
            FrameLayout emptyAlbums = findViewById(R.id.noAlbum_Frame);
            emptyAlbums.setVisibility(View.VISIBLE);
        }
    }

    public void createNewAlbum(View v) {
        System.out.println("YO THIS IS WORKING! LOVE!");
    }
}
