package com.rutcs.clp.photos27_android;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class AlbumManager {
    //The defined structure of each album is as follows aName, path;path;path;path...
    private static HashMap<String,String> maAlbums = new HashMap<>();

    private AlbumManager(){}

    public static void readAlbums(String path, Context context) {
        try {
            File file = new File(path);
            if(!file.exists()){
                file.createNewFile();
                return;
            }
            InputStream inputStream = context.openFileInput(path);

            if (inputStream != null) {
                InputStreamReader isr = new InputStreamReader(inputStream);
                BufferedReader br = new BufferedReader(isr);
                String rstring = "";
                StringBuilder sb = new StringBuilder();

                while ((rstring = br.readLine())!=null) {
                    sb.append(rstring);
                }
                isr.close();
                String albumString = sb.toString();
                maAlbums.put(albumString.split(":")[0], albumString.split(":")[1]);
            }

        } catch (IOException ex) {

            ex.printStackTrace();
        }
    }

    public static boolean albumsExist() {
        return !maAlbums.isEmpty();
    }


}
